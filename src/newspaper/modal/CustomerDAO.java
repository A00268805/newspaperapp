 package newspaper.modal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import newspaper.connection.GetConnection;



public class CustomerDAO {
	GetConnection db = new GetConnection();
	ArrayList<customer> al = new ArrayList<customer>();
	
	public String insertData(String name,String contactno,String deliveryarea,String address){
		String temp ="";
		String sql = "insert into Customer(C_name,Contact_no,Delivery_area,Address)values(?,?,?,?)";
		Connection con = db.getConn();
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, contactno);
			stmt.setString(2, deliveryarea);
			stmt.setString(2, address);
			stmt.executeUpdate();
			temp = "success";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return temp;
	}
	//.........show....................
	public ArrayList<customer> showData(){
		String sql = "select * from user";
		Connection con = db.getConn();
		try {
			//Statement stmt = con.createStatement();
			PreparedStatement ps = con.prepareStatement(sql);
		    ResultSet rs =	ps.executeQuery(sql);
		    while (rs.next()) {
		    	customer customer = new customer();
				String name = rs.getString("C_name");
				String contactno = rs.getString("Contact_no");
				String deliveryarea = rs.getString("Delivery_area");
				String address = rs.getString("Address");
				
				customer.setName(name);
				customer.setContactno(contactno);
				customer.setDeliveryarea(deliveryarea);
				customer.setAddress(address);
				al.add(customer);
			}
		    
			for(int i=0;i<al.size();i++){
				System.out.println(al.get(i).getName()+ "    "+al.get(i).getContactno()+ "    "+al.get(i).getDeliveryarea()+ "    "+al.get(i).getAddress());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return al;
	}
	
	//.................delete.............
	
	public String delData(String delname){
		
		String temp = "";
		String sql = "delete from user where C_name=?";
		
		Connection con = db.getConn();
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, delname);
			ps.execute();
			temp = "success";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return temp;
	}


}
