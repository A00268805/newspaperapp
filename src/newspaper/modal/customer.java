package newspaper.modal;

public class customer {
	private int id;
	private String name;
	private String contactno;
	private String deliveryarea;
	private String address;
	
	public customer() {
		super();
	}
	
	public customer(String name) {
		super();
		this.name = name;
	}

	public customer(String name, String contactno) {
		super();
		this.name = name;
		this.contactno = contactno;
	}
	public customer(String name, String contactno,String deliveryarea) {
		super();
		this.name = name;
		this.contactno = contactno;
		this.deliveryarea=deliveryarea;
	}
	public customer(String name, String contactno,String deliveryarea,String address) {
		super();
		this.name = name;
		this.contactno = contactno;
		this.deliveryarea=deliveryarea;
		this.address=address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactno() {
		return contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	}

	public String getDeliveryarea() {
		return deliveryarea;
	}

	public void setDeliveryarea(String deliveryarea) {
		this.deliveryarea = deliveryarea;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	
	
	

}
