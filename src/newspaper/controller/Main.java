package newspaper.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import newspaper.modal.CustomerDAO;

/**
 * Servlet implementation class Main
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Main() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		String name = request.getParameter("name");
		String contactno = request.getParameter("contactno");
		String deliveryarea = request.getParameter("deliveryarea");
		String address = request.getParameter("address");
		CustomerDAO dao = new CustomerDAO();
		
		String s1 = dao.insertData(name,contactno, deliveryarea, address);
		if(s1.equals("success")){
			out.println("data is inserted");
			
		}else{
			out.println("some thing is going wrong");
		}
		
		//...............Second Form fatch data from data base.............
		
	String delname = request.getParameter("delname");
	
	String s2 = dao.delData(delname);
	out.println(s2);
	if(s2.equals("success")){
		out.println("data is delete");
		response.sendRedirect("index.jsp");
	}else{
		out.println("some thing is going wrong");
	}
	
	}

}
